<?php get_header();?>

<style>
    .logo-yellow,.brown,.logo-violet,.logo-black,.white{
        display: none;
    }
</style>


<section class="bannerHome">
    <?php echo do_shortcode('[rev_slider alias="home"][/rev_slider]') ?>
</section>

<section class="marqueeContenedor">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <div class="containerMarquee">
                <div class="marquee">
                    <div class="marquee__inner first">
                        
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                    
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</section>

<section class="balancedCare">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-6 sinPadding">
                <div class="text">
                    <h2>VONT HEALTHY <br> BALANCED CARE</h2>
                    <p>Es una marca que piensa en tu salud completa y equilibrada al conjugar el cuidado que le das a tu cuerpo, espíritu y mente.</p>
                </div>
            </div>
            <div class="col-md-6 sinPadding">
                    <div class="image"></div>
            </div>
        </div>
    </div>
</section>

<section class="loHacemosPorTi">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 offset-lg-6 offset-md-5">
                <div class="text">
                    <h2>LO HACEMOS <br> POR TI</h2>
                    <p>En Vont buscamos ofrecerte una conexión contigo mismo.</p>
                    <p>Nuestros productos y servicios están enfocados para que cada uno mejore su entorno al mejorar primero consigo mismo.</p>
                    <p>No importa tu área de necesidad o mejora (física, mental o espiritual), o si buscas es tu belleza interna o externa.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="marcaHogar">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-6">
                <h2>UNA MARCA QUE PUEDES <br> LLAMAR TU HOGAR</h2>
                <p>Un espacio para guiarte, preguntarte y ayudarte en tu vida diaria por medio de nuestros especialistas y productos a encontrar respuestas por ti mismo a esos aspectos de tu vida que deseas mejorar.</p>
                <a href="" class="btn-black">SABER MÁS</a>
            </div>
        </div>
    </div>
</section>

<section class="marqueeContenedor">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <div class="containerMarquee">
                <div class="marquee">
                    <div class="marquee__inner first">
                        
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                    
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</section>

<section class="mejoraTuVida">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <h2>MEJORA TU VIDA</h2>
            </div>
            <div class="col-md-8 sinPadding">
                <div class="principal">
                    <div class="infoPrincipal">
                        <h3 class="title">Mental VONT</h3>
                        <p>Conoce como funciona tu cerebro y mente y porqué es importante en los resultados que tenemos en nuestras vidas.</p>
                        <a href="<?php echo home_url('mental');?>">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sinPadding">
                <a href="<?php echo home_url('body');?>" class="category one">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/index/bodyVont.png' ?>" alt="VONT | Body Vont" class="img-fluid logoCategory">
                </a>

                <a href="<?php echo home_url('spirit');?>" class="category two">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/index/vonSpirit.png' ?>" alt="VONT | VONT Spirit" class="img-fluid logoCategory">
                </a>

                <a href="<?php echo home_url('beauty');?>" class="category three">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/index/beautyVont.png' ?>" alt="VONT | Beauty Vont"  class="img-fluid logoCategory">
                </a>

            </div>
        </div>
    </div>
</section>

<section class="marqueeContenedor">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <div class="containerMarquee">
                <div class="marquee">
                    <div class="marquee__inner first">
                        
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                        <span><i>VONT HEALTHY BALANCED CARE</i></span>
                        <span><i> Soluciones para sueño, dolor y ansiedad </i></span>
                        <span><i> Fortalece tu cuerpo </i></span>
                        <span><i> Conecta con tu plan de vida </i>
                    
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</section>


<section class="ecommerce">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-xl-4 offset-xl-1">
                <div class="bannerEcommerce">
                    <div class="info">
                        <h3>COMPLEMENTA TU <br> BELLEZA CON NUESTRA <br> LÍNEA DE PRODUCTOS</h3>
                        <p>Descubre todos los productos que VONT <br> tiene para ti</p>
                        <a href="https://tienda.vont.mx" class="btn-black" >Ver todos</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-7 sinPadding">
                <div class="sliderProducts">
                    <div class="owl-carousel owl-theme" id="sliderProducts">

                        <?php
                            $args = array(
                                'post_type' => 'productos',
                                'posts_per_page' => -1
                            );

                            $q = new WP_Query($args);
                        ?>
                        <?php while($q->have_posts()): $q->the_post() ?>

                            <div class="item" style="background-image: url('<?php the_post_thumbnail_url();?>')">

                                <!-- INFO CATEORY -->
                                <div class="category">
                                    <div class="text">
                                        <p><?php the_field('categoria');?></p>
                                        <p><?php the_field('subtitulo');?></p>
                                    </div>
                                    <label class="arrow"><i class="fas fa-long-arrow-alt-right"></i></label>
                                </div>

                                <!-- PRICE -->
                                <div class="price">
                                    <a href="<?php the_field('url');?>" target="_blank"><?php the_title();?></a>
                                    <a href="<?php the_field('url');?>" target="_blank"><?php the_field('precio');?> --- Añadir al carrito</a>
                                </div>
                                
                            </div>

                        <?php endwhile ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="familiaVont">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
            <h2>CONOCE A LA FAMILIA VONT</h2>
            </div>
        </div>
    </div>

    <?php
        $args = array(
            'post_type' => 'family',
            'posts_per_page' => -1
        );

        $q = new WP_Query($args);
    ?>
    
    <div class="owl-carousel owl-theme" id="familyvont">

        <?php while($q->have_posts()): $q->the_post() ?>

            <div class="item">
                <a href="#">
                    <img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
                    <div class="inner">
                        <a class="namePerson" href="#"><?php the_title();?></a>

                            <?php while( have_rows('redes_sociales') ): the_row() ?>
                                <a href="<?php the_sub_field('url');?>"><i class="<?php the_sub_field('icono');?>"></i></a>
                            <?php endwhile; ?>
    
                    </div>
                </a>
            </div>

        <?php endwhile ?>
        
</section>

<section class="contacto">
    <div class="title">
        <p>¿Algo más que te gustaría saber?</p>
        <h2>CONTÁCTANOS</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>DEJA QUE CUIDEMOS <br> TU CUERPO Y ESPÍRITU.</h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>





<?php get_footer();?>
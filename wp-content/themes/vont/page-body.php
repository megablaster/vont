<?php get_header(); ?>

<style>
    .morado{
        background-color: #4d4c58;
    }

    .logo-header,.brown,.logo-violet,.logo-black{
        display: none;
    }

    .white{
        display: inline-block;
    }

    .site-header{
        position: absolute;
        top:0;
        left:0;
        width:100%;
        background-color: transparent;
        border-bottom: 1px solid #fff;
        z-index: 9999;

    }

    .site-header .contactoHeader{
        border-left: 1px solid #fff;
    }

    .site-header a{
        color: #fff;
    }

    .logo-brown{
        display: none;
    }

    .offcanvas .offcanvas-body .itemsCanvas ul li a{
        font-family: 'Bebas Neue', cursive;
    }

    .top-button a{
        background-color: #4d4c58;
    }
</style>

<section class="bannerBody">
    <div class="container">
        <div class="row">
            <div class="offset-lg-6 col-lg-6">
                <div class="textBanner">
                    <h1>Body Vont</h1>
                    <p>Tu cuerpo es salud, acompáñalo con nutrición, descanso, relajación, meditación y respiración.</p>
                    <a class="btn-white" href="contacto">CONTACTO</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="riquezaBody">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-7">
                <div class="text">
                    <p>CON VONT OBTENDRÁS LA MAYOR DE LAS RIQUEZAS</p>
                    <h2>El cuerpo es el instrumento <br> <i>del alma</i></h2>
                    <p>La salud balanceada es la base de todo.</p>
                    <p>Sin salud no tienen sentido muchas de nuestros logros y objetivos.</p>
                    <p>Podemos alcanzar muchos éxitos laborales y/o financieros, pero sin salud y por ende felicidad su logro puede resultar contradictorio o inútil.</p>
                    <p>Sin salud no tienen sentido muchas de nuestros logros y objetivos.</p>
                    <p>Podemos alcanzar muchos éxitos laborales y/o financieros, pero sin salud y por ende felicidad su logro puede resultar contradictorio o inútil.</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="image">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/body/coach.jpg' ?> " alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section class="tipsTranformarVida">
    <div class="banner">
        <h2>Llegó el momento de transformar tu vida</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/body/tip2.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Calmar tu mente</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus voluptatibus vel unde molestias esse sequi eligendi in corporis enim nemo.</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/body/tip3.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Conexión con todo</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo asperiores quod aperiam necessitatibus delectus iure soluta labore dolor. Sint, asperiores?</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/body/tip1.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Enfoque en tu plan de vida</h4>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perspiciatis, rerum distinctio. Blanditiis eius delectus ut perferendis, veniam ex maiores officiis.</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="definirPoyectoBody">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col-lg-7">
                <div class="text">
                    <h2>DEFINE TU PROYECTO <br> <i>DE VIDA</i></h2>
                    <p>Plan de vida principal</p>
                </div>

                <div class="accordion">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                ¿Qué es eso que más quiero?
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor nam laborum inventore dicta est placeat dignissimos, ea, quam quis beatae quidem illum natus accusamus ullam impedit a sit! Reiciendis, ducimus.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                ¿Qué es lo que me apasiona?
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius, voluptates!
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ¿Qué es aquello que me quita el sueño?
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, soluta.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                ¿Para qué soy bueno?
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, expedita.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                ¿Cuáles son mis dones y talentos?
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, laboriosam!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 sinPadding">
                <div class="imgAccordion">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/body/planDeVidaCoach.jpg' ?>" alt="" class="img-fluid">
                    <div class="info">
                        <h4>FUERZA Y PAZ</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla cumque quibusdam rem non officiis? Voluptatem ratione repudiandae laboriosam ullam ut?</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="conocerVontBody">
    <div class="container-fluid">
        <h3>Conoce que más te ofrece VONT</h3>
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div class="category">
                    <div class="owl-carousel owl-theme" id="sliderCategoryBody">

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category1.jpg')">
                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Spirit</a>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium, voluptatum!</p>
                                <a href="" class="moreInfo">Saber más ___</a>
                            </div>
                            
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tenetur, perferendis.</p>
                                <a href="" class="moreInfo">Saber más ___</a>
                            </div>
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, eius!</p>
                                <a href="" class="moreInfo">Saber más ___</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blogBody">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding  morado">
                <p>NUESTRO BLOG</p>
                <h2>FORTALECE TU CUERPO <br> CON NUESTROS CONSEJOS</h2>
            </div>

            <div class="col-md-12">
                <div class="contentBlog">
                    <div class="owl-carousel owl-theme" id="blog">

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog2.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog1.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newsLetterBody">	
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h3>Únete a nuestro <br> newsletter </h3>
            </div>
            <div class="col-lg-8">
                <div class="formNewsLetter">
                    <form action="">
                        <input type="email" name="" id="" placeholder="EMAIL">
                        <input type="submit" value="ENVIAR">
                        <label for=""><input type="checkbox" name="" id=""> Aceptar términos y condiciones</label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="familiaVontPurple">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <p>Logros VONT</p>
                <h2>Conviértete en <i>uno de ellos</i> _______</h2>
            </div>
        </div>
    </div>
    
    <div class="owl-carousel owl-theme" id="familyvont">
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_book_cover.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
</section>

<section class="logrosBody">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="image" style="background-image: url('../img/mental/slider1Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider2Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider3Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contactoBody">
    <div class="titlePath">
        <h2>¿QUIERES SER <br> MIEMBRO?</h2>
        <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/vont-logo-white.png' ?>" alt="" class="img-fluid">
    </div>

    <div class="title">
        <p>¨¿Algo más que te gustaría saber?</p>
        <h2>CONTÁCTANOS</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>DEJA QUE CUIDEMOS <br> TU CUERPO Y ESPÍRITU.</h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
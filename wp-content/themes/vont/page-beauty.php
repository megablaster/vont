<?php get_header();?>

<style>
    .logo-yellow,.logo-violet,.logo-header,.isotipo,.logo-brown{
        display: none;
    }

    .brown{
        display: inline-block;
    }

    .carmin{
        background-color: #c7b6ad;
    }

    .brownStrong{
        background-color: #513615;
    }

    .brownStrong .white{
        display: inline-block;
    }

    .brownStrong .isotipo{
        display: inline-block;
    }

    .footer footer p {
        font-family: 'Abril Fatface', cursive;
    }

    .site-header{
        position: absolute;
        top:0;
        left:0;
        width:100%;
        background-color: transparent;
        border-bottom: 1px solid #655648;
        z-index: 9999;

    }

    .site-header a{
        color: #655648;
    }

    .offcanvas .offcanvas-body .itemsCanvas ul li a{
        font-family: 'Abril Fatface', cursive;
    }

    .top-button a{
        background-color: #000;
    }
    
</style>



<section class="bannerBeauty">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="textBanner">
                    <h1>VONT Spirit</h1>
                    <p>Transmite tu esencia personal, enfoque y energía</p>
                    <a class="btn-white" href="contacto">CONTACTO</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="productsBeauty">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 sinPadding headerProductsBeauty">
                <p>NOVEDADES</p>
                <h2>Productos destacados</h2>
            </div>

            <div class="col-md-12">
                <div class="contentProducts">
                    <div class="owl-carousel owl-theme" id="productBeauty">

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                                <!-- info -->
                                <div class="info">
                                    <div class="text">
                                        <p>-Dias y Noche</p>
                                        <p>-Serum</p>
                                        <p>-3 uds. Goteador 10 ml</p>
                                        <p>-Todo tipo de pieles</p>
                                        <p>-Rostro</p>
                                        <a href="" class="moreInfo">Comprar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <p class="leyend">GEL FLUIDO BOTOX LIKE</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product2.png')"></div>
                                <!-- info -->
                                <div class="info">
                                    <div class="text">
                                        <p>-Dias y Noche</p>
                                        <p>-Serum</p>
                                        <p>-3 uds. Goteador 10 ml</p>
                                        <p>-Todo tipo de pieles</p>
                                        <p>-Rostro</p>
                                        <a href="" class="moreInfo">Comprar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="nameProduct">
                                <h3 class="name">COLOR DAY CREMA HIDRATANTE</h3>
                                <p class="leyend">TODO LO QUE TU PIEL NECESITA EN UN ÚNICO PRODUCTO</p>
                            </div>  
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product3.png')"></div>
                                <!-- info -->
                                <div class="info">
                                    <div class="text">
                                        <p>-Dias y Noche</p>
                                        <p>-Serum</p>
                                        <p>-3 uds. Goteador 10 ml</p>
                                        <p>-Todo tipo de pieles</p>
                                        <p>-Rostro</p>
                                        <a href="" class="moreInfo">Comprar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="nameProduct">
                                <h3 class="name">HD MASK DETOX & OXYGEN</h3>
                                <p class="leyend">MASCARILLA EXFOLIANTE Y OXIGENANTE</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product2.png')"></div>
                                <!-- info -->
                                <div class="info">
                                    <div class="text">
                                        <p>-Dias y Noche</p>
                                        <p>-Serum</p>
                                        <p>-3 uds. Goteador 10 ml</p>
                                        <p>-Todo tipo de pieles</p>
                                        <p>-Rostro</p>
                                        <a href="" class="moreInfo">Comprar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="nameProduct">
                                <h3 class="name">COLOR DAY CREMA HIDRATANTE</h3>
                                <p class="leyend">TODO LO QUE TU PIEL NECESITA EN UN ÚNICO PRODUCTO</p>
                            </div>  
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="tipsBeauty">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="text1">
                    <p>CUIDA TU PIEL CON LO MEJOR DE VONT</p>
                    <h2>POR UNA PIEL SANA <br> Y RADIANTE</h2>
                    <p class="info">Escucha y mira los testimonios de mucha gente que ha usado las marcas que Vont distribuye y ha beneficiado su cuerpo de manera positiva, desde el cuidado de la piel hasta tener más energía en el día a día.</p>
                    <a href="" class="btn-black">Saber más</a>
                </div>
                
            </div>
            <div class="col-lg-6 sinPadding">
                <div class="image">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/pielSana.png' ?> " alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row align-items-center">
        <div class="col-lg-6 order-xl-1 order-lg-1 order-2 sinPadding">
                <div class="image">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/cabello.png' ?> " alt="">
                </div>
            </div>
            <div class="col-lg-6 order-xl-2 order-lg-2 order-1">
                <div class="text2">
                    <p>LOS MEJORES PRODUCTOS PARA EL CUIDADO DE TU CABELLO ESTÁN AQUÍ</p>
                    <h2>RENUEVA TU CABELLO <br> CON Beauty Vont</h2>
                    <p class="info">Escucha y mira los testimonios de mucha gente que ha usado las marcas que Vont distribuye y ha beneficiado su cuerpo de manera positiva, desde el cuidado de la piel hasta tener más energía en el día a día.</p>
                    <a href="" class="btn-black">Saber más</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-5">
                <div class="text3">
                    <p class="info">Escucha y mira los testimonios de mucha gente que ha usado las marcas que Vont distribuye y ha beneficiado su cuerpo de manera positiva, desde el cuidado de la piel hasta tener más energía en el día a día.</p>
                    <a href="" class="btn-black">Saber más</a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="imagetext3">
                    <div class="content">
                        <p>CUIDA TU PIEL CON LO MEJOR DE VONT</p>
                        <h2>POR UN ROSTRO RADIENATE <br> Y LLENO DE VIDA</h2>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/rostro.png' ?> " alt="">
                </div>
            </div>
            
        </div>
    </div>
</section>

<section class="blogBeauty">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding blogCarmin">
                <p>NUESTRO BLOG</p>
                <h2>FORTALECE TU ESPÍRITU <br> CON ESTOS TIPS</h2>
            </div>

            <div class="col-md-12">
                <div class="contentBlog">
                    <div class="owl-carousel owl-theme" id="blog">

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog2.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog1.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newsLetterBeauty">	
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h3>Únete a nuestro <br> newsletter </h3>
            </div>
            <div class="col-lg-8">
                <div class="formNewsLetter">
                    <form action="">
                        <input type="email" name="" id="" placeholder="EMAIL">
                        <input type="submit" value="ENVIAR">
                        <label for=""><input type="checkbox" name="" id=""> Aceptar términos y condiciones</label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="conocerVontBeauty">
    <div class="container-fluid">
        <h3>Conoce que más te ofrece VONT</h3>
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div class="category">
                    <div class="owl-carousel owl-theme" id="sliderCategory">

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category1.jpg')">
                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Spirit</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                            
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="familiaVontBeauty">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <p>Logros VONT</p>
                <h2>Conviértete en uno de ellos _______</h2>
            </div>
        </div>
    </div>
    
    <div class="owl-carousel owl-theme" id="familyvont">
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_book_cover.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
</section>

<section class="logrosBeauty">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="image" style="background-image: url('../img/mental/slider1Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider2Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider3Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contactoBeauty">
    <div class="titlePath">
        <h2>¿QUIERES SER <br> MIEMBRO?</h2>
        <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/vont-logo-white.png' ?>" alt="" class="img-fluid">
    </div>

    <div class="title">
        <p>¨¿Algo más que te gustaría saber?</p>
        <h2>CONTÁCTANOS</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>DEJA QUE CUIDEMOS <br> TU CUERPO Y ESPÍRITU.</h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>
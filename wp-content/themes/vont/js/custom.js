jQuery(function (jQuery) {

    var header = jQuery(".site-header");
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 150) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    });

    //Asociados
    

   //Owl slider Iddex
    jQuery('#sliderProducts').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: true,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            700:{
                items:2
            },
            1000:{
                items:2	
            }
        }
    });

    jQuery('#sliderCategory').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            700:{
                items:1
            },
            1000:{
                items:2	
            }
        }
    });

    jQuery('#sliderCategoryBody').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            700:{
                items:1
            },
            1000:{
                items:1	
            }
        }
    });

    jQuery('#Ayudar').owlCarousel({
        stagePadding: 200,
        autoplayHoverPause:true,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        items:1,
        lazyLoad: true,
        nav:true,
        responsive:{
            0:{
                items:1,
                stagePadding: 60
            },
            600:{
                items:1,
                stagePadding: 100
            },
            1000:{
                items:1,
                stagePadding: 200
            },
            1200:{
                items:1,
                stagePadding: 250
            },
            1400:{
                items:1,
                stagePadding: 300
            },
            1600:{
                items:1,
                stagePadding: 350
            },
            1800:{
                items:1,
                stagePadding: 400
            }
        }
    });

    jQuery('#familyvont').owlCarousel({
        stagePadding: 200,
        autoplayHoverPause:true,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        items:1,
        lazyLoad: true,
        nav:true,
        responsive:{
            0:{
                items:1,
                stagePadding: 60
            },
            600:{
                items:1,
                stagePadding: 100
            },
            1000:{
                items:1,
                stagePadding: 200
            },
            1200:{
                items:1,
                stagePadding: 250
            },
            1400:{
                items:1,
                stagePadding: 300
            },
            1600:{
                items:1,
                stagePadding: 350
            },
            1800:{
                items:1,
                stagePadding: 400
            }
        }
    });

    jQuery('#sliderVertical').owlCarousel({
        stagePadding: 200,
        autoplayHoverPause:true,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        items:1,
        lazyLoad: true,
        nav:true,
        responsive:{
            0:{
                items:1,
                stagePadding: 60
            },
            600:{
                items:1,
                stagePadding: 100
            },
            1000:{
                items:1,
                stagePadding: 200
            },
            1200:{
                items:1,
                stagePadding: 250
            },
            1400:{
                items:1,
                stagePadding: 300
            },
            1600:{
                items:1,
                stagePadding: 350
            },
            1800:{
                items:1,
                stagePadding: 400
            }
        }
    });


    jQuery('#blog').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            700:{
                items:2
            },
            1000:{
                items:3	
            }
        }
    });

    jQuery('#productBeauty').owlCarousel({
        loop:true,
        margin:40,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            700:{
                items:2
            },
            1000:{
                items:3	
            }
        }
    });

    jQuery('#productAsociados').owlCarousel({
        loop:true,
        margin:30,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            700:{
                items:3
            },
            1000:{
                items:4	
            },
            1500:{
                items:5	
            }
        }
    });

    //Carousel vertical
    (function ($) {
        jQuery(document).ready(function () {
            // init
            $('.cv-carousel').carouselVertical({
                items: 1,
                margin: 10,
                loop: true,
                center:true,
                autoplay:true
            });
            // for moving programmatically the carousel
            // you can do that
            $('.cv-carousel').trigger('goTo', [0]);
           
        });
    })(jQuery);


}); // jQuery End

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }





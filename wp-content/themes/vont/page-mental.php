<?php get_header();?>
<style>
    .verde{
        background-color: #122323;
    }

    .logo-header,.brown,.logo-violet,.logo-black{
        display: none;
    }

    .white{
        display: inline-block;
    }

    .site-header{
        position: absolute;
        top:0;
        left:0;
        width:100%;
        background-color: transparent;
        border-bottom: 1px solid #fff;
        z-index: 9999;

    }

    .site-header .contactoHeader{
        border-left: 1px solid #fff;
    }

    .site-header a{
        color: #fff;
    }

    .logo-brown{
        display: none;
    }

</style>

<section class="bannerMental">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="textBanner">
                    <h1>Healthy Care</h1>
                    <p>El mejor para tu cuerpo y mente</p>
                    <a class="btn-white" href="contacto">CONTACTO</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="riqueza">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-7">
                <div class="text">
                    <p>CON VONT OBTENDRÁS LA MAYOR DE LAS RIQUEZAS</p>
                    <h2>LA PRIMERA RIQUEZA <br> ES LA SALUD</h2>
                    <p>La salud balanceada es la base de todo.</p>
                    <p>Sin salud no tienen sentido muchas de nuestros logros y objetivos.</p>
                    <p>Podemos alcanzar muchos éxitos laborales y/o financieros, pero sin salud y por ende felicidad su logro puede resultar contradictorio o inútil.</p>
                    <p>Nuestra salud se construye desde el primer pensamiento y acción del día, pasa por lo que pensamos, sentimos y hacemos en favor o en contra de nuestro cuerpo.</p>
                    <p>Vont ofrece productos para mejorar algunos aspectos de tu salud debido al estrés de nuestros días y el ritmo de vida tan demandante que vivimos hoy en día.</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="image">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/doctor.jpg' ?> " alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pleca">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="path">
                    <h2 class="textMask">LLEGÓ EL MOMENTO <br> DE MEJORAR TU VIDA</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ayuda">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
            <h2>TE PODEMOS AYUDAR <label>____________</label></h2>
            </div>
        </div>
    </div>
    
    <div class="owl-carousel owl-theme" id="Ayudar">

        <?php while( have_rows('te_podemos_ayudar','option') ): the_row() ?>

            <div class="item">
                <a href="#">
                    <img src="<?php the_sub_field('imagen');?>" class="img-fluid">
                    <div class="inner">
                        <div class="logo">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/vont-logo-brown.png' ?> " alt="">
                        </div>
                        <div class="text">
                            <h4><?php the_sub_field('titulo');?></h4>
                            <p><?php the_sub_field('descripcion');?> _______</p>
                            <a class="btn-veige" href="<?php the_sub_field('url');?>" target="_blank">SABER MÁS</a>
                        </div>
                    </div>
                </a>
            </div>

        <?php endwhile; ?>
        
</section>

<section class="tips">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/calma.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Calmar tu mente</h4>
                        <a href="">Saber más___</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/conexion.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Conexión con todo</h4>
                        <a href="">Saber más___</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tip">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/planVida.jpg' ?>" alt="" class="img-fluid">
                    <div class="infoTip">
                        <h4>Enfoque en tu plan de vida</h4>
                        <a href="">Saber más___</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="definirPoyecto">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="text">
                <h2>DEFINIR TU PLAN <br> DE VIDA</h2>
                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-8 col-8  sinPadding">
                <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/definirProyecto.jpg' ?>" alt="" class="img-fluid">
            </div>
            <div class="col-xl-5 col-lg-5 col-md-4 col-4 sinPadding">
                <div class="imgAccordion">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/definirProyecto2.jpg' ?>" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-7">
                <div class="accordion">
                    <div class="accordion" id="accordionExample">

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                ¿Qué es eso que más quiero?
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor nam laborum inventore dicta est placeat dignissimos, ea, quam quis beatae quidem illum natus accusamus ullam impedit a sit! Reiciendis, ducimus.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                ¿Qué es lo que me apasiona?
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius, voluptates!
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ¿Qué es aquello que me quita el sueño?
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, soluta.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                ¿Para qué soy bueno?
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, expedita.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                ¿Cuáles son mis dones y talentos?
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, laboriosam!
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="conocerVont">
    <div class="container-fluid">
        <h3>Conoce que más te ofrece VONT</h3>
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div class="category">
                    <div class="owl-carousel owl-theme" id="sliderCategory">

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category1.jpg')">
                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Spirit</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                            
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                        </div>

                        <!-- IMG CATEGORY -->
                        <div class="item" style="background-image: url('../img/mental/category2.jpg')">

                            <!-- info -->
                            <div class="info">
                                <a href="" class="title">Body</a>
                                <a href="" class="moreInfo">Más Información ___</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blogMental">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding verde">
                <p>NUESTRO BLOG</p>
                <h2>CONOCE LOS CONSEJOS <br> PARA MEJORAR TU SALUD</h2>
            </div>

            <div class="col-md-12 veige">
                <div class="contentBlog">
                    <div class="owl-carousel owl-theme" id="blog">

                        <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'category',
                                        'field'    => 'slug',
                                        'terms'    => array('mental'),
                                    ),
                                ),
                            );
                            $q = new WP_Query($args);
                        ?>

                        <?php while($q->have_posts()): $q->the_post() ?>

                            <div class="item">
                                <div class="img" style="background-image: url('<?php the_post_thumbnail_url();?>')"></div>
                                <div class="info">
                                    <p class="date"><?php echo get_the_date();?></p>
                                    <h4><?php the_title();?></h4>
                                    <p><?php the_excerpt();?></p>
                                    <a href="<?php the_permalink();?>" class="moreInfo">LEER MÁS</a>
                                </div>
                            </div>

                        <?php endwhile ?>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="newsLetter">	
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h3>Únete a nuestro <br> newsletter </h3>
            </div>
            <div class="col-lg-8">
                <div class="formNewsLetter">
                    <form action="">
                        <input type="email" name="" id="" placeholder="EMAIL">
                        <input type="submit" value="ENVIAR">
                        <label for=""><input type="checkbox" name="" id=""> Aceptar términos y condiciones</label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="familiaVontGreen">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <p>Logros VONT</p>
                <h2>Conviértete en uno de ellos <span>_______</span></h2>
            </div>
        </div>
    </div>

    <?php
        $args = array(
            'post_type' => 'family',
            'posts_per_page' => -1
        );

        $q = new WP_Query($args);
    ?>
    
    <div class="owl-carousel owl-theme" id="familyvont">

        <?php while($q->have_posts()): $q->the_post() ?>

            <div class="item">
                <a href="#">
                    <img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
                    <div class="inner">
                            <a class="namePerson" href="#"><?php the_title();?></a>
                            <?php while( have_rows('redes_sociales') ): the_row() ?>
                                <a href="<?php the_sub_field('url');?>"><i class="<?php the_sub_field('icono');?>"></i></a>
                            <?php endwhile; ?>
    
                    </div>
                </a>
            </div>
            
    <?php endwhile ?>
        
</section>

<section class="logros">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="image" style="background-image: url('../img/mental/slider1Logoros.jpg');"></div>
                            <div class="carousel-caption  d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                        <div class="image" style="background-image: url('../img/mental/slider2Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                        <div class="image" style="background-image: url('../img/mental/slider3Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contactoVerde">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="titlePath">
                    <h2>¿QUIERES SER <br> MIEMBRO?</h2>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/vont-logo-white.png' ?>" alt="" class="img-fluid">
                </div>

                <div class="title">
                    <p>¨¿Algo más que te gustaría saber?</p>
                    <h2>CONTÁCTANOS</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>DEJA QUE CUIDEMOS <br> TU CUERPO Y ESPÍRITU.</h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>
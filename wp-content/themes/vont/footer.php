<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

<section class="footer verde morado violetFooter carmin">
  <footer>
    <div class="container">
      <div class="row">

        <div class="col-xl-3">
          <img src="<?php echo get_stylesheet_directory_uri().'/img/index/logo-vont.png' ?>" alt="VONT | LOGOTIPO" class="img-fluid logo-brown" >
          <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/logo-vont-yellow.png' ?>" alt="VONT | LOGOTIPO" class="img-fluid logo-yellow" >
          <img src="<?php echo get_stylesheet_directory_uri().'/img/spirit/logo-vont-violet.png' ?>" alt="VONT | LOGOTIPO" class="img-fluid logo-violet">
          <img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/logo-vont-black.png' ?>" alt="VONT | LOGOTIPO" class="img-fluid logo-black">
        </div>

        <div class="col-xl-9">
          <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-6">
              <p>ACERCA DE NOSOTROS /</p>
              <ul>
                <li><a href="">Historia</a></li>
                <li><a href="">FQA</a></li>
                <li><a href="">Contactos</a></li>
              </ul>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
              <p>BY VONT /</p>
              <ul>
                <li><a href="">Body</a></li>
                <li><a href="">Spirit</a></li>
                <li><a href="">Beauty</a></li>
                <li><a href="">Accesories</a></li>
              </ul>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
              <p>PLANES DE VIDA /</p>
              <ul>
                <li><a href="">Plan de vida 1</a></li>
                <li><a href="">Plan de vida 2</a></li>
                <li><a href="">Plan de vida 3</a></li>
                <li><a href="">Plan de vida 4</a></li>
              </ul>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
              <p>COMUNIDAD /</p>
              <p><a href="">LOGROS /</a></p>
              <p><a href="">OFERTAS ESPECIALES /</a></p>
              <div class="social">
                <a href="">Facebook /</a>
                <a href="">Instagram</a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </footer>
</section>

<div class="top-button position-fixed zi-1020">
  <a href="#to-top" class="btn shadow"><i class="fas fa-chevron-up"></i></a>
</div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
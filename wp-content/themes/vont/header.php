<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/safari-pinned-tab.svg" color="#0d6efd">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
  <?php wp_body_open(); ?>

  <div id="to-top"></div>

  <div id="page" class="site">

    <header id="masthead absolute" class="site-header">

      <nav class="nav">
          <div class="container-fluid">
            <div class="row align-items-center">

                  <div class="col-xl-3 col-lg-4 col-md-5 col-sm-6 col-6 order-xl-1 order-lg-1 order-md-1 order-2">
                    <div class="row">
                      <div class="col-md-6">
                          <a class="menu" data-bs-toggle="offcanvas" data-bs-target="#offcanvasTop" aria-controls="offcanvasTop">
                            <i class="fas fa-bars"></i> <labeL>MENÚ</labeL></a>
                      </div>
                      <div class="col-md-6 hide">
                          <?php if (is_active_sidebar('sidebar-lang')):?>
                              <?php dynamic_sidebar('sidebar-lang')?>
                          <?php endif?>
                      </div>
                    </div>
                  </div>

                  <div class="col-xl-6 col-lg-4 col-md-2 col-sm-6 col-6 text-center order-xl-2 order-lg-2 order-md-2 order-1">
                    <a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri().'/img/index/logo-header.png' ?>" alt="VONT" class="img-fluid logo-header"></a>
                    <a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri().'/img/index/logo-header-white.png' ?>" alt="VONT" class="img-fluid logo-header white"></a>
                    <a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/logo-header-brown.png' ?>" alt="VONT" class="img-fluid logo-header brown"></a>
                  </div>

                  <div class="col-xl-3 col-lg-4 col-md-5 order-3 hide">
                    <div class="row align-items-center">
                      <div class="col-md-6 ">
                      <a href=""><img src="<?php echo get_stylesheet_directory_uri().'/img/index/isotipo-vont.png' ?>" alt="VONT" class="img-fluid isotipo"></a>
                      <a href=""><img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/isotipo-brown.png' ?>" alt="VONT" class="img-fluid isotipo brown"></a>
                      </div>
                      <div class="col-md-6 contactoHeader">
                          <a href="">CONTACTO</a>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
      </nav>

      <div class="offcanvas offcanvas-top verde morado violet brownStrong grey" tabindex="-1" id="offcanvasTop" aria-labelledby="offcanvasTopLabel">
        <div class="offcanvas-header border">
          <div class="container-fluid">
              <div class="row align-items-center">
                  <div class="col-xl-3 col-lg-4 col-md-5 order-xl-1 order-lg-1 order-md-1 order-2 ">
                    <div class="row align-items-center">
                      <div class="col-md-6 col-6">
                          <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"> <i class="fas fa-times"></i> <label> CERRAR</label> </button> 
                      </div>
                      <div class="col-md-6 col-6">
                        <div class="lenguage">
                            <?php if (is_active_sidebar('sidebar-lang')):?>
                                <?php dynamic_sidebar('sidebar-lang')?>
                            <?php endif?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xl-6 col-lg-4 col-md-2 order-xl-2 order-lg-2 order-md-2 order-1  text-center">
                    <a href=""><img src="<?php echo get_stylesheet_directory_uri().'/img/index/logo-header-white.png' ?>" alt="VONT" class="img-fluid logo-header"></a>
                    <a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri().'/img/index/logo-header-white.png' ?>" alt="VONT" class="img-fluid logo-header white"></a>
                  </div>

                  <div class="col-xl-3 col-lg-4 col-md-5 order-3 ">
                    <div class="row align-items-center">
                      <div class="col-md-6 hide">
                      <a href=""><img src="<?php echo get_stylesheet_directory_uri().'/img/index/isotipo-vont.png' ?>" alt="VONT" class="img-fluid isotipo"></a>
                      </div>
                      <div class="col-md-6 contactoHeader hide">
                          <a href="">CONTACTO</a>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
        <div class="offcanvas-body">
          <div class="container">
            <div class="row">
              <div class="col-lg-6">
                  <div class="itemsCanvas">
                    <ul>
                      <li><label>(01)</label><a href="<?php echo home_url('/');?>">VONT /</a></li>
                      <li><label>(02)</label><a href="<?php echo home_url('mental');?>">MENTAL /</a></li>
                      <li><label>(03)</label><a href="<?php echo home_url('body');?>">BODY /</a></li>
                      <li><label>(04)</label><a href="<?php echo home_url('spirit');?>">SPIRIT /</a></li>
                      <li><label>(05)</label><a href="<?php echo home_url('beauty');?>">BEAUTY /</a></li>
                      <li><label>(06)</label><a href="<?php echo home_url('asociados');?>">ASOCIADOS /</a></li>
                      <li><label>(07)</label><a href="<?php echo home_url('nosotros');?>">ACERCA DE /</a></li>
                    </ul>
                  </div>
                  <div class="contact">
                    <p>Llamanos <a href="tel:525512345678">+52 (55) 1234 5678</a></p>
                    <p>Síguenos <a href="">Instagram</a> <a href="">Facebook</a></p>
                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="articles">

                    <a class="postArticle">
                      <label>( CONOCE MÁS SOBRE LO QUE VONT TE OFRECE )</label>
                      <h2 class="title">VONT</h2>
                      <div class="imagePost" style="background-image: url('../img/index/post1.png')"></div>
                    </a>

                    <a class="postArticle">
                      <label>( LA MEJOR ALTERNTIVA PARA CUIDAR TU MENTE )</label>
                      <h2 class="title">MENTAL</h2>
                      <div class="imagePost"  style="background-image: url('../img/index/post2.png')"></div>
                    </a>

                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header><!-- #masthead -->


    <!-- <?php
                wp_nav_menu(array(
                  'theme_location' => 'main-menu',
                  'container' => false,
                  'menu_class' => '',
                  'fallback_cb' => '__return_false',
                  'items_wrap' => '<ul id="bootscore-navbar" class="navbar-nav ms-auto %2$s">%3$s</ul>',
                  'depth' => 2,
                  'walker' => new bootstrap_5_wp_nav_menu_walker()
                ));
                ?> -->

    <?php bootscore_ie_alert(); ?>
<?php get_header();?>

<style>

    body{
        bacground-color: #e7eaed;
    }

    .violet{
        background-color: #594fb8;
    }

    .violetFooter{
        background-color:#a3b3d4;
    }

    .footer footer p {
        font-family: 'Belleza', sans-serif;
    }

    .logo-header,.brown,.logo-yellow,.logo-black{
        display: none;
    }


    .white{
        display: inline-block;
    }

    .site-header{
        position: absolute;
        top:0;
        left:0;
        width:100%;
        background-color: transparent;
        border-bottom: 1px solid #fff;
        z-index: 9999;

    }

    .site-header .contactoHeader{
        border-left: 1px solid #fff;
    }

    .site-header a{
        color: #fff;
    }

    .logo-brown{
        display: none;
    }

    .offcanvas .offcanvas-body .itemsCanvas ul li a{
        font-family: 'Belleza', sans-serif;
    }

    .top-button a{
        background-color: #4d4c58;
    }
</style>

<section class="bannerSpirit">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="textBanner">
                    <h1>VONT Spirit</h1>
                    <p>Transmite tu esencia personal, enfoque y energía</p>
                    <a class="btn-white" href="contacto">CONTACTO</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="riquezaSpirit">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="text">
                    <p>CON VONT OBTENDRÁS LA MAYOR DE LAS RIQUEZAS</p>
                    <h2>De altos espíritus es <br> <b> aspirar a cosas altas </b></h2>
                    <p>¿Algunas ves te has preguntado, que es el espíritu? O alma u otro nombre que recibe en varias culturas.</p>
                    <p>¿Qué relación tiene el espíritu con nuestro cuerpo y mente? ¿Cuál de ellos es más importante para ti?</p>
                    <p>¿Por qué decimos en Vont “Healthy Care”?</p>
                    <p>El espíritu ha sido muy importante desde culturas milenarias y como es utilizado el día de hoy en la vida moderna.</p>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="image">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/spirit/coach.png' ?> " alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section class="claseTaller">
    <div class="container-fluid">
        <div class="row align-items-start">

            <div class="col-lg-7">
                <div class="text">
                    <h2>Llegó el momento de  <br> <b>transformar tu vida</b></h2>
                </div>

                <div class="accordion">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Clase o Taller
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor nam laborum inventore dicta est placeat dignissimos, ea, quam quis beatae quidem illum natus accusamus ullam impedit a sit! Reiciendis, ducimus.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Clase o Taller
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius, voluptates!
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Clase o Taller
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, soluta.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                Clase o Taller
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, expedita.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                Clase o Taller
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, laboriosam!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 sinPadding">
                <div class="imgAccordion">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/spirit/maestra.png' ?>" alt="" class="img-fluid">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="planDeVida">
    <div class="container">
        <div class="text">
                <p>Juntos trabajaremos en</p>
                <h2>Definir tu proyecto de vida</h2>
            </div>
        <div class="row">
            <div class="col-md-12">
            <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#question1" type="button" role="tab" aria-controls="pills-home" aria-selected="true">¿Qué es eso que más quiero?</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#quiestion2" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">¿Qué es lo que me apasiona?</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#quiestion3" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">¿Qué es aquello que me quita el sueño?</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#quiestion4" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">¿Para qué soy bueno?</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#quiestion5" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">¿Cuáles son mis dones y talentos?</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="question1" role="tabpanel" aria-labelledby="pills-home-tab">
                            <p>¿Qué es eso que más quiero?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum nihil tempora rem repudiandae optio quidem ab in sunt natus maxime.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor pariatur impedit cumque, consequatur repellat nulla atque iusto provident veniam eos!</p>
                            <a href="">Saber más</a>
                        </div>
                        <div class="tab-pane fade" id="quiestion2" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <p>¿Qué es lo que me apasiona?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum nihil tempora rem repudiandae optio quidem ab in sunt natus maxime.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor pariatur impedit cumque, consequatur repellat nulla atque iusto provident veniam eos!</p>
                            <a href="">Saber más</a>
                        </div>
                        <div class="tab-pane fade" id="quiestion3" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <p>¿Qué es aquello que me quita el sueño?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum nihil tempora rem repudiandae optio quidem ab in sunt natus maxime.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor pariatur impedit cumque, consequatur repellat nulla atque iusto provident veniam eos!</p>
                            <a href="">Saber más</a>
                        </div>
                        <div class="tab-pane fade" id="quiestion4" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <p>¿Para qué soy bueno?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum nihil tempora rem repudiandae optio quidem ab in sunt natus maxime.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor pariatur impedit cumque, consequatur repellat nulla atque iusto provident veniam eos!</p>
                            <a href="">Saber más</a>
                        </div>
                        <div class="tab-pane fade" id="quiestion5" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <p>Cuáles son mis dones y talentos?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum nihil tempora rem repudiandae optio quidem ab in sunt natus maxime.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor pariatur impedit cumque, consequatur repellat nulla atque iusto provident veniam eos!</p>
                            <a href="">Saber más</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</section>

<section class="categorySpirit">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 sinPadding">
                <div class="category" style="background-image: url('../img/spirit/category1.jpg')">
                    <div class="infoCategory">
                        <h4>Category Title</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus voluptatibus vel unde molestias esse sequi eligendi in corporis enim nemo.</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sinPadding">
                <div class="category" style="background-image: url('../img/spirit/category3.jpg')">
                    <div class="infoCategory">
                        <h4>Category Title</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo asperiores quod aperiam necessitatibus delectus iure soluta labore dolor. Sint, asperiores?</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sinPadding">
                <div class="category" style="background-image: url('../img/spirit/category2.jpg')">
                    <div class="infoCategory">
                        <h4>Category Title</h4>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perspiciatis, rerum distinctio. Blanditiis eius delectus ut perferendis, veniam ex maiores officiis.</p>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blogSpirit">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding blogviolet">
                <p>NUESTRO BLOG</p>
                <h2>FORTALECE TU ESPÍRITU <br> CON ESTOS TIPS</h2>
            </div>

            <div class="col-md-12">
                <div class="contentBlog">
                    <div class="owl-carousel owl-theme" id="blog">

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog2.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog1.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>

                        <div class="item">
                            <!-- IMG blog -->
                            <div class="img" style="background-image: url('../img/mental/blog3.jpg')"></div>
                            <!-- info -->
                            <div class="info">
                                <p class="date">23/12/2021</p>
                                <h4>Title</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente similique dolore qui ducimus quasi saepe cupiditate ad nobis iste amet quia perspiciatis ipsam quo quibusdam aut, aliquam dolor rem commodi?</p>
                                <a href="" class="moreInfo">LEER MÁS</a>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newsLetterSpirit">	
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h3>Únete a nuestro <br> newsletter </h3>
            </div>
            <div class="col-lg-8">
                <div class="formNewsLetter">
                    <form action="">
                        <input type="email" name="" id="" placeholder="EMAIL">
                        <input type="submit" value="ENVIAR">
                        <label for=""><input type="checkbox" name="" id=""> Aceptar términos y condiciones</label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="familiaVontViolet">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <p>Logros VONT</p>
                <h2>Conviértete en <i>uno de ellos</i> _______</h2>
            </div>
        </div>
    </div>
    
    <div class="owl-carousel owl-theme" id="familyvont">
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_book_cover.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="#">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/slider_bags.jpg" alt="" />
                <div class="inner">
                    <a class="namePerson" href="#">NAME PERSON</a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                    <a href=""><i class="fab fa-twitter"></i></a>
                </div>
            </a>
        </div>
</section>

<section class="logrosSpirit">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="image" style="background-image: url('../img/mental/slider1Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider2Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider3Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contactoSpirit">
    <div class="titlePath">
        <h2>¿QUIERES SER <br> MIEMBRO?</h2>
        <img src="<?php echo get_stylesheet_directory_uri().'/img/mental/vont-logo-white.png' ?>" alt="" class="img-fluid">
    </div>

    <div class="title">
        <p>¨¿Algo más que te gustaría saber?</p>
        <h2>CONTÁCTANOS</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>DEJA QUE CUIDEMOS <br> TU CUERPO Y ESPÍRITU.</h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>





<?php get_footer();?>
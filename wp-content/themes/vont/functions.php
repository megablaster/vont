<?php

// style and scripts
add_action('wp_enqueue_scripts', 'bootscore_child_enqueue_styles');
function bootscore_child_enqueue_styles() {

  // style.css
  wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
  
  //Splide css
  //wp_enqueue_style( 'owl-css', get_stylesheet_directory_uri() . '/plugins/splide/css/splide.min.css' );

  // style owlSlider
  wp_enqueue_style( 'owl-css', get_stylesheet_directory_uri() . '/plugins/owl/assets/owl.carousel.min.css' );
  wp_enqueue_style( 'owl-theme-css', get_stylesheet_directory_uri() .'/plugins/owl/assets/owl.theme.default.min.css');

  // Compiled Bootstrap
  $modified_bootscoreChildCss = date('YmdHi', filemtime(get_stylesheet_directory() . '/css/lib/bootstrap.min.css'));
  wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/lib/bootstrap.min.css', array('parent-style'), $modified_bootscoreChildCss);
 
  //font Awesome
  wp_enqueue_script( 'font-js', 'https://kit.fontawesome.com/14823deefc.js', false, '', true );

  //Splide js
  //wp_enqueue_script('splide-js', get_stylesheet_directory_uri().'/plugins/splide/js/splide.min.js', false, '', true );
 
  // custom.js
  wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true );

  //js owl slider
  wp_enqueue_script( 'owl-js', get_stylesheet_directory_uri() . '/plugins/owl/owl.carousel.min.js', false, '', true );

  // custom.js
  wp_enqueue_script('carousel-vertical', get_stylesheet_directory_uri() . '/js/jquery.carousel-vertical.min.js', false, '', true );
}

//Widgets
function wpb_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Language sidebar', 'bootscore' ),
		'id' => 'sidebar-lang',
		'description' => __( 'Sidebar de idioma', 'bootscore' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

}

add_action( 'widgets_init', 'wpb_widgets_init' );


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Secciones',
		'menu_title'	=> 'Secciones',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}
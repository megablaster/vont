<?php get_header();?>

<style>
    .logo-yellow,.logo-violet,.logo-black,.brown,.white{
        display: none;
    }

    .grey{
        background-color: #464646;
    }
</style>

<section class="bannerAsociados">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="textBanner">
                    <h1>SALUD FÍSICA, MENTAL Y ESPIRITUAL A TU ALCANCE CON VONT HEALTHY BALANCED CARE</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="beneficios">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 sinPadding">
                <div class="category">
                    <div class="content">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/beneficio1.jpg' ?>" alt="" class="img-fluid">
                        <div class="infoCategory">
                            <h4>Category Title</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus voluptatibus vel unde molestias esse sequi eligendi in corporis enim nemo.</p>

                        </div>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/icon1.png' ?>" alt="" class="img-fluid icon">
                </div>
            </div>
            <div class="col-lg-4 sinPadding">
                <div class="category">
                    <div class="content">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/beneficio2.jpg' ?>" alt="" class="img-fluid">
                        <div class="infoCategory">
                            <h4>Category Title</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo asperiores quod aperiam necessitatibus delectus iure soluta labore dolor. Sint, asperiores?</p>

                        </div>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/icon2.png' ?>" alt="" class="img-fluid icon">
                </div>
            </div>
            <div class="col-lg-4 sinPadding">
                <div class="category">
                    <div class="content">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/beneficio3.jpg' ?>" alt="" class="img-fluid">
                        <div class="infoCategory">
                            <h4>Category Title</h4>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perspiciatis, rerum distinctio. Blanditiis eius delectus ut perferendis, veniam ex maiores officiis.</p>

                        </div>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/icon3.png' ?>" alt="" class="img-fluid icon">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="distribuidor">
    <div class="container">
        <div class="row align-items-end margin">
            <div class="col-md-4">
                <div class="img">
                <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/productosDistribuidor.png' ?>" alt="" class="img-fluid"> 
                </div>
            </div>
            <div class="col-md-8">
                <div class="content">
                    <h3>¿Buscas ser proveedor de producto de calidad que cuiden <br> <i>la salud física, mental y espiritual?</i></h3>
                    <p>Estás en el lugar indicado</p>
                    <a href="">Quiero ser distribuidor</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="serDistribuidor">
    <div class="container">
        <div class="title">
            <h2>CONVIÉRTETE EN <br> <i> DISTRIBUIDOR VONT</i></h2>
            <p>Y descubre seis formas de obtener ingresos con VONT Healthy balanced care</p>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor1.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Ventas Minoristas</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor2.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Bonificaciones Semanales</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor3.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Incentivos</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor4.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Bono igualable Vitalicio</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor5.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Bono de Élite</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="content">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/asociados/distribuidor6.jpg' ?>" alt="">
                    <div class="text">
                        <h3>Bono de Liderazgo</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, nesciunt!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="escalaNegocio">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-10">
                <div class="text">
                    <h2>ESCALA <i> TU NEGOCIO </i></h2>
                    <p>Con VONT tienes el poder de crear un negocio cuyo éxito depende completamente de ti. Tú eres el jefe. Trabajas cuando quieres (tanto o tan poco como quieras) y ganas dinero compartiendo aquello que amas. Tienes el control, pero no estás solo. Tienes acceso a una red de apoyo que te proporciona herramientas, formación y conocimiento para que puedas tener éxito.</p>
                    <a href="">Quiero saber más</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="categoryProductos">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-11">
                <h2>CONOCE <i>LO QUE OFRECEMOS</i></h2>
            </div>
            <div class="col-md-8 sinPadding">
                <div class="principal">
                    <div class="infoPrincipal">
                        <h3 class="title">SUPLEMENTOS <br> VITAMÍNICOS</h3>
                        <a href="">Saber más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sinPadding">
                <a href="" class="category one">
                    <span>NUTRICIONALES</span>
                </a>

                <a href="" class="category two">
                    <span>MALLAS DE COMPRESIÓN DE ÚLTIMA GENERACIÓN</span>
                </a>

                <a href="" class="category three">
                    <span>SPRAY REVITALIZANTE</span>
                </a>
            </div>
        </div>
    </div>
</section>


<section class="productosAsociados">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 sinPadding headerProductsBeauty">
                <h2>PRODUCTOS <i>DESTACADOS</i></h2>
            </div>

            <div class="col-md-11">
                <div class="contentProducts">
                    <div class="owl-carousel owl-theme" id="productAsociados">

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="content">
                                <!-- IMG PRODUCT -->
                                <div class="img" style="background-image: url('../img/beauty/product1.png')"></div>
                            </div>
                            <div class="nameProduct">
                                <h3 class="name">BOTULIFT SÉRUM</h3>
                                <h4 class="price">EUR 35.00</h4>
                                <p class="leyend">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="verticalSlider">
    <div class="background">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="info">
                        <h3>Ofrecemos productos de la más alta calidad, lo que significa el mayor valor para TU SALUD.</h3>
                        <p>La mayoría de los productos vitamínicos están hechos de sustancias químicas sintéticas que el cuerpo no reconoce y no puede procesar, y de minerales en formas que el cuerpo no puede usar.</p>
                        <p>Nosotros nos enfocamos en tener los productos naturales de mejor calidad para ofrecer solo lo mejor para ti y tu salud.</p>
                        <a href="">Quiero ser Distribuidor</a>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">

                    <div class="sp-asociados">
                        <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide">Slide 01</li>
                                    <li class="splide__slide">Slide 02</li>
                                    <li class="splide__slide">Slide 03</li>
                                </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="letreroAsociados">
    <div class="row">
        <div class="col-md-12">
            <h2>FORMA PARTE DE <br> <i>ALGO MÁS GRANDE</i></h2>
            <img src="<?php echo get_stylesheet_directory_uri().'/img/beauty/isotipo-brown.png' ?>" alt="" class="img-fuid">
        </div>
    </div>
</section>

<section class="logrosAsociados">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 sinPadding">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="image" style="background-image: url('../img/asociados/logro-asociados.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider2Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image" style="background-image: url('../img/mental/slider3Logoros.jpg');"></div>
                            <div class="carousel-caption d-md-block">
                                <p>Actualmente</p>
                                <h5>VONT <i>ha logrado</i> ____</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, delectus sunt. Ratione, quidem architecto! Aperiam..</p>
                                <a href="" class="btn-white">SABER MÁS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contactoAsociados">

    <div class="title">
        <p>¨¿Algo más que te gustaría saber?</p>
        <h2>CONTÁCTANOS</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="map">
                    <div class="info">
                        <div class="text">
                            <p>HAZ CLICK AQUÍ PARA VER NUESTRAS UBICACIONES EN GOOGLE MAPS</p>
                            <p>+52 (55) 1234 5678</p>
                        </div>
                        <label><i class="fas fa-map-marker-alt"></i></label>
                    </div>
                    <h3>VISÍTANOS</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form">
                    <h3>FORMA PARTE DE <br> <i>ALGO MÁS GRANDE</i></h3>
                    <?php echo do_shortcode('[contact-form-7 id="8" title="Contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>







<?php get_footer();?>